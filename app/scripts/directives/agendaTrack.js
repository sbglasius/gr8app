angular.module('gr8appApp')
  .directive('agendaTrack', function() {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      templateUrl: 'views/partials/directives/_track.html'
    };
  });
