'use strict';

app
    .controller('BlogCtrl', ['$scope', 'blogService', function ($scope, blogService) {
        function getBlogs(offset) {
            blogService.getBlogs(3, offset, function (data) {
                $scope.offset = offset;
                $scope.posts = data.posts;
                $scope.showNewer = $scope.offset > 0;
                $scope.showOlder = $scope.offset + 3 < data.blog.posts;
            });
        }
        getBlogs(0);
        $scope.showBlogs = function(offset) {
            getBlogs(offset)
        }
    }]);
