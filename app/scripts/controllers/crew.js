app
    .controller('CrewCtrl', ['$scope', function ($scope) {
        $scope.leader = {
            name: 'Søren Berg Glasius',
            employer: "Openminds ApS",
            link: 'http://openminds.dk',
            image: 'soeren.png',
            twitter: 'sbglasius',
            email: 'sbglasius@gr8conf.org'
        };

        var crewsters = [
            {
                name: 'Brian Johnsen',
                employer: "Gennemtænkt IT",
                link: 'http://gennemtaenkt.dk',
                image: 'brian.png',
                twitter: 'brianjohnsendk ',
                email: 'brian@gr8conf.org'
            },
            {
                name: 'Morten Kristiansen',
                employer: "Gennemtænkt IT",
                link: 'http://gennemtaenkt.dk',
                image: 'morten.png',
                twitter: 'mlkristiansen',
                email: 'morten@gr8conf.org'
            },
            {
                name: 'Jacob Aae Mikkelsen',
                employer: "Gennemtænkt IT",
                link: 'http://gennemtaenkt.dk',
                image: 'jacob.png',
                twitter: 'JacobAae',
                email: 'jacob@gr8conf.org'
            },
            {
                name: 'Aage Nielsen',
                employer: "Openminds ApS",
                link: 'http://gennemtaenkt.dk',
                image: 'aage.png',
                twitter: 'aagnie',
                email: 'aage@gr8conf.org'
            },
            {
                name: 'Jon Bæk Bomme',
                employer: "1337 IT ApS",
                link: 'http://www.linkedin.com/in/jonbomme',
                image: 'jon.png',
                twitter: 'jonbomme',
                email: 'jon@gr8conf.org'
            },
            {
                name: 'Thomas Borg Salling',
                employer: "Freelance",
                link: 'http://tbsalling.dk/',
                image: 'thomas.png',
                twitter: 'tbsalling',
                email: 'thomas@gr8conf.org'
            },
            {
                name: 'Niels Ull Harremoës',
                employer: "Null Consult ApS",
                link: 'http://www.linkedin.com/in/nielsull',
                image: 'niels.png',
                twitter: 'Null_dk',
                email: 'niels@gr8conf.org'
            },
            {
                name: 'Henning Hørslev Hansen',
                employer: "LeksIT-DocuBizz ApS",
                link: 'http://leksit.dk/',
                image: 'henning.png',
                twitter: 'henninghhansen',
                email: 'henning@gr8conf.org'
            },
            {
                name: 'Christian von Wendt-Jensen',
                employer: "Freelance",
                link: 'http://dk.linkedin.com/pub/christian-von-wendt-jensen/0/675/12',
                image: 'christian.png',
                twitter: 'cwendtjensen',
                email: 'christian@gr8conf.org'
            }
        ];


        var n = 3;
        var crew = _.chain(crewsters).groupBy(function (element, index) {
            return Math.floor(index / n);
        }).toArray().value();
        $scope.crew = crew;
    }]);
