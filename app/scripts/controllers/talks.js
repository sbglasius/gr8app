app
    .controller('TalksCtrl', ['$scope', 'storage', 'FEATURES', function ($scope, storage, FEATURES) {

        $scope.$on('loaded', function (event, args) {
            if (FEATURES.talks && args[0] === "talks") {
                $scope.talks = args[1]
            }
        });
        $scope.talks = storage.get('talks');
        $scope.$watch('talks', function () {
            if(!$scope.talks) return;
            var n = 3;
            $scope.talkGroups = _.chain($scope.talks).groupBy(function (element, index) {
                return Math.floor(index / n);
            }).toArray().value();
        })

    }]);