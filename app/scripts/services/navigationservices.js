'use strict';

app
    .service('scrollService', ['$window','Angularytics',function ($window, Angularytics) {
        this.scrollTo = function (id, category, title) {
            angular.element(document).scrollTo(document.getElementById(id), 120, 2000);
            Angularytics.trackEvent(category, title)
        };
    }])
    .service('registerService', ['Angularytics',function (Angularytics) {
        this.registerClick = function (category, title) {
            Angularytics.trackEvent(category, title);
        };
    }]);
